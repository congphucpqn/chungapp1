//
//  ViewController.swift
//  FlurryBlankApp
//
//  Created by Phu Pham Cong on 1/15/18.
//  Copyright © 2018 Phu Pham Cong. All rights reserved.
//

import UIKit

class ViewController: UIViewController, AmazonAdViewDelegate {
    func viewControllerForPresentingModalView() -> UIViewController! {
        return self
    }
    
    func adViewDidLoad(_ view: AmazonAdView!) -> Void {
        self.view.addSubview(view)
    }
    
    @IBOutlet weak var ipLabel: UILabel!
    @IBOutlet weak var amzKeyLbl: UILabel!
    @IBOutlet var amazonAdView: AmazonAdView!
    @IBOutlet var amzView2: AmazonAdView!
    @IBOutlet var amzView3: AmazonAdView!
    @IBOutlet var amzView4: AmazonAdView!
    @IBOutlet var amzView5: AmazonAdView!
    @IBOutlet var amzView6: AmazonAdView!
    @IBOutlet var amzView7: AmazonAdView!
    @IBOutlet var amzView8: AmazonAdView!
    @IBOutlet var amzView9: AmazonAdView!
    @IBOutlet var amzView10: AmazonAdView!
    @IBOutlet var amzView11: AmazonAdView!
    @IBOutlet var amzView12: AmazonAdView!
    @IBOutlet var amzView13: AmazonAdView!
    @IBOutlet var amzView14: AmazonAdView!
    @IBOutlet var amzView15: AmazonAdView!
    @IBOutlet var amzView16: AmazonAdView!
    @IBOutlet var amzView17: AmazonAdView!
    @IBOutlet var amzView18: AmazonAdView!
    @IBOutlet var amzView19: AmazonAdView!
    @IBOutlet var amzView20: AmazonAdView!
    @IBOutlet var amzView21: AmazonAdView!
    @IBOutlet var amzView22: AmazonAdView!
    @IBOutlet var amzView23: AmazonAdView!
    @IBOutlet var amzView24: AmazonAdView!
    var timer: Timer!
    
    @IBOutlet weak var webView: UIWebView!
    override func viewDidLoad() {
        super.viewDidLoad()
        amzKeyLbl.text = AppDelegate.AMZ_AD_KEY
        loadAmazonAds()
        timer = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(ViewController.loadAmazonAds), userInfo: nil, repeats: true)
        ipLabel.text = getWiFiAddress()
    }
    
    
    @objc func loadAmazonAds() {
        // Initialize Auto Ad Size View
        let adFrame: CGRect = CGRect(x:0, y:20, width:320, height:50);
        let adFrame2: CGRect = CGRect(x:0, y:adFrame.origin.y + 50, width:320, height:50);
        let adFrame3: CGRect = CGRect(x:0, y:adFrame2.origin.y + 50, width:320, height:50);
        let adFrame4: CGRect = CGRect(x:0, y:adFrame3.origin.y + 50, width:320, height:50);
        let adFrame5: CGRect = CGRect(x:0, y:adFrame4.origin.y + 50, width:320, height:50);
        let adFrame6: CGRect = CGRect(x:0, y:adFrame5.origin.y + 50, width:320, height:50);
        let adFrame7: CGRect = CGRect(x:0, y:adFrame6.origin.y + 50, width:320, height:50);
        let adFrame8: CGRect = CGRect(x:0, y:adFrame7.origin.y + 50, width:320, height:50);
        let adFrame9: CGRect = CGRect(x:0, y:adFrame8.origin.y + 50, width:320, height:50);
        let adFrame10: CGRect = CGRect(x:0, y:adFrame9.origin.y + 50, width:320, height:50);
        let adFrame11: CGRect = CGRect(x:0, y:adFrame10.origin.y + 50, width:320, height:50);
        let adFrame12: CGRect = CGRect(x:0, y:adFrame11.origin.y + 50, width:320, height:50);
        let adFrame13: CGRect = CGRect(x:160, y:20, width:320, height:50);
        let adFrame14: CGRect = CGRect(x:160, y:adFrame.origin.y + 50, width:320, height:50);
        let adFrame15: CGRect = CGRect(x:160, y:adFrame2.origin.y + 50, width:320, height:50);
        let adFrame16: CGRect = CGRect(x:160, y:adFrame3.origin.y + 50, width:320, height:50);
        let adFrame17: CGRect = CGRect(x:160, y:adFrame4.origin.y + 50, width:320, height:50);
        let adFrame18: CGRect = CGRect(x:160, y:adFrame5.origin.y + 50, width:320, height:50);
        let adFrame19: CGRect = CGRect(x:160, y:adFrame6.origin.y + 50, width:320, height:50);
        let adFrame20: CGRect = CGRect(x:160, y:adFrame7.origin.y + 50, width:320, height:50);
        let adFrame21: CGRect = CGRect(x:160, y:adFrame8.origin.y + 50, width:320, height:50);
        let adFrame22: CGRect = CGRect(x:160, y:adFrame9.origin.y + 50, width:320, height:50);
        let adFrame23: CGRect = CGRect(x:160, y:adFrame10.origin.y + 50, width:320, height:50);
        let adFrame24: CGRect = CGRect(x:160, y:adFrame11.origin.y + 50, width:320, height:50);
        
        
        amazonAdView = AmazonAdView.init(adSize: AmazonAdSize_320x50)
        amazonAdView.frame = adFrame
        amzView2 = AmazonAdView.init(adSize: AmazonAdSize_320x50)
        amzView2.frame = adFrame2
        amzView3 = AmazonAdView.init(adSize: AmazonAdSize_320x50)
        amzView3.frame = adFrame3
        amzView4 = AmazonAdView.init(adSize: AmazonAdSize_320x50)
        amzView4.frame = adFrame4
        amzView5 = AmazonAdView.init(adSize: AmazonAdSize_320x50)
        amzView5.frame = adFrame5
        amzView6 = AmazonAdView.init(adSize: AmazonAdSize_320x50)
        amzView6.frame = adFrame6
        amzView7 = AmazonAdView.init(adSize: AmazonAdSize_320x50)
        amzView7.frame = adFrame7
        amzView8 = AmazonAdView.init(adSize: AmazonAdSize_320x50)
        amzView8.frame = adFrame8
        amzView9 = AmazonAdView.init(adSize: AmazonAdSize_320x50)
        amzView9.frame = adFrame9
        amzView10 = AmazonAdView.init(adSize: AmazonAdSize_320x50)
        amzView10.frame = adFrame10
        amzView11 = AmazonAdView.init(adSize: AmazonAdSize_320x50)
        amzView11.frame = adFrame11
        amzView12 = AmazonAdView.init(adSize: AmazonAdSize_320x50)
        amzView12.frame = adFrame12
        amzView13 = AmazonAdView.init(adSize: AmazonAdSize_320x50)
        amzView13.frame = adFrame13
        amzView14 = AmazonAdView.init(adSize: AmazonAdSize_320x50)
        amzView14.frame = adFrame14
        amzView15 = AmazonAdView.init(adSize: AmazonAdSize_320x50)
        amzView15.frame = adFrame15
        amzView16 = AmazonAdView.init(adSize: AmazonAdSize_320x50)
        amzView16.frame = adFrame16
        amzView17 = AmazonAdView.init(adSize: AmazonAdSize_320x50)
        amzView17.frame = adFrame17
        amzView18 = AmazonAdView.init(adSize: AmazonAdSize_320x50)
        amzView18.frame = adFrame18
        amzView19 = AmazonAdView.init(adSize: AmazonAdSize_320x50)
        amzView19.frame = adFrame19
        amzView20 = AmazonAdView.init(adSize: AmazonAdSize_320x50)
        amzView20.frame = adFrame20
        amzView21 = AmazonAdView.init(adSize: AmazonAdSize_320x50)
        amzView21.frame = adFrame21
        amzView22 = AmazonAdView.init(adSize: AmazonAdSize_320x50)
        amzView22.frame = adFrame22
        amzView23 = AmazonAdView.init(adSize: AmazonAdSize_320x50)
        amzView23.frame = adFrame23
        amzView24 = AmazonAdView.init(adSize: AmazonAdSize_320x50)
        amzView24.frame = adFrame24
        
        
        
        // Register the ViewController with the delegate to receive callbacks.
        amazonAdView.delegate = self
        amzView2.delegate = self
        amzView3.delegate = self
        amzView4.delegate = self
        amzView5.delegate = self
        amzView6.delegate = self
        amzView7.delegate = self
        amzView8.delegate = self
        amzView9.delegate = self
        amzView10.delegate = self
        amzView11.delegate = self
        amzView12.delegate = self
        amzView13.delegate = self
        amzView14.delegate = self
        amzView15.delegate = self
        amzView16.delegate = self
        amzView17.delegate = self
        amzView18.delegate = self
        amzView19.delegate = self
        amzView20.delegate = self
        amzView21.delegate = self
        amzView22.delegate = self
        amzView23.delegate = self
        amzView24.delegate = self
        
        // Set Ad Loading Options & Load Ad
        let options = AmazonAdOptions()
        amazonAdView.loadAd(options)
        amzView2.loadAd(options)
        amzView3.loadAd(options)
        amzView4.loadAd(options)
        amzView5.loadAd(options)
        amzView6.loadAd(options)
        amzView7.loadAd(options)
        amzView8.loadAd(options)
        amzView9.loadAd(options)
        amzView10.loadAd(options)
        amzView11.loadAd(options)
        amzView12.loadAd(options)
        amzView13.loadAd(options)
        amzView14.loadAd(options)
        amzView15.loadAd(options)
        amzView16.loadAd(options)
        amzView17.loadAd(options)
        amzView18.loadAd(options)
        amzView19.loadAd(options)
        amzView20.loadAd(options)
        amzView21.loadAd(options)
        amzView22.loadAd(options)
        amzView23.loadAd(options)
        amzView24.loadAd(options)
    }
    
    // Return IP address of WiFi interface (en0) as a String, or `nil`
    func getWiFiAddress() -> String? {
        var address : String?
        
        // Get list of all interfaces on the local machine:
        var ifaddr : UnsafeMutablePointer<ifaddrs>?
        guard getifaddrs(&ifaddr) == 0 else { return nil }
        guard let firstAddr = ifaddr else { return nil }
        
        // For each interface ...
        for ifptr in sequence(first: firstAddr, next: { $0.pointee.ifa_next }) {
            let interface = ifptr.pointee
            
            // Check for IPv4 or IPv6 interface:
            let addrFamily = interface.ifa_addr.pointee.sa_family
            if addrFamily == UInt8(AF_INET) || addrFamily == UInt8(AF_INET6) {
                
                // Check interface name:
                let name = String(cString: interface.ifa_name)
                if  name == "en0" {
                    
                    // Convert interface address to a human readable string:
                    var hostname = [CChar](repeating: 0, count: Int(NI_MAXHOST))
                    getnameinfo(interface.ifa_addr, socklen_t(interface.ifa_addr.pointee.sa_len),
                                &hostname, socklen_t(hostname.count),
                                nil, socklen_t(0), NI_NUMERICHOST)
                    address = String(cString: hostname)
                }
            }
        }
        freeifaddrs(ifaddr)
        
        return address
    }
}

